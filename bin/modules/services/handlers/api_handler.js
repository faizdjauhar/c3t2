'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const validator = require('../utils/validator');
const queryHandler = require('../repositories/queries/query_handler');
const commandHandler = require('../repositories/commands/command_handler');

const getServices = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParam(queryParam);

  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await queryHandler.getServices(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}

const getOneServices = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParam(queryParam);

  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await queryHandler.getOneServices(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}

const postOneServices = async (req, res, next) => {
  const payload = req.body;
  const validateParam = await validator.isValidParam(payload);

  const postRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.postOneServices(payload);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`, 200);
  }

  sendResponse(await postRequest(validateParam));
}

const putOneServices = async (req, res, next) => {
  const id = req.params;
  const payload = req.body;
  const validateParam = await validator.isValidParam(payload);

  const putRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.putOneServices(id, payload);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`, 200);
  }
  
  sendResponse(await putRequest(validateParam));
}

const delOneServices = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParam(queryParam);

  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.delOneServices(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}

module.exports = {
  getServices: getServices,
  getOneServices: getOneServices,
  postOneServices: postOneServices,
  putOneServices: putOneServices,
  delOneServices: delOneServices
}
'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const validator = require('../utils/validator');
const queryHandler = require('../repositories/queries/query_handler');
const commandHandler = require('../repositories/commands/command_handler');

const getBantuan = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParam(queryParam);

  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await queryHandler.getBantuan(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}

const getOneBantuan = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParam(queryParam);

  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await queryHandler.getOneBantuan(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}

const postOneBantuan = async (req, res, next) => {
  const payload = req.body;
  const validateParam = await validator.isValidParam(payload);

  const postRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.postOneBantuan(payload);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`, 200);
  }

  sendResponse(await postRequest(validateParam));
}

const putOneBantuan = async (req, res, next) => {
  const id = req.params;
  const payload = req.body;
  const validateParam = await validator.isValidParam(payload);

  const putRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.putOneBantuan(id, payload);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`, 200);
  }
  
  sendResponse(await putRequest(validateParam));
}

const deleteOneBantuan = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParam(queryParam);

  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.deleteOneBantuan(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}

module.exports = {
  getBantuan: getBantuan,
  getOneBantuan: getOneBantuan,
  postOneBantuan: postOneBantuan,
  putOneBantuan: putOneBantuan,
  deleteOneBantuan: deleteOneBantuan
}
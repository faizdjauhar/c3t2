'use strict';

const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');

class Subscribes{
    constructor(param){
        this.idSubscribes = param.idSubscribes;
        this.idUser = param.idUser;
        this.serviceId = param.serviceId;
        this.renewTotal = param.renewTotal;
        this.createdAt = param.createdAt;
        this.activeUntil = param.activeUntil;
        this.updatedAt = param.updatedAt;
    }

    async viewSubscribes(){
        const param = {};
        const result = await query.findSubscribes(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }

    async viewOneSubscribes(){
        const param = {"idSubscribes":this.idSubscribes};
        const result = await query.findOneSubscribes(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }

    async viewSubscribesByIdUser(){
        const param = {"idUser":this.idUser};
        const result = await query.findSubscribesByIdUser(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }
}

module.exports = Subscribes;
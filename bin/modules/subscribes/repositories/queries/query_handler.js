'use strict';

const Subscribes = require('./domain');

const getSubscribes = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const subscribes = new Subscribes(queryParam);
        const result = await subscribes.viewSubscribes();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}

const getSubscribesByIdUser = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const subscribes = new Subscribes(queryParam);
        const result = await subscribes.viewSubscribesByIdUser();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}

const getOneSubscribes = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const subscribes = new Subscribes(queryParam);
        const result = await subscribes.viewOneSubscribes();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}

module.exports = {
    getSubscribes : getSubscribes,
    getOneSubscribes : getOneSubscribes,
    getSubscribesByIdUser: getSubscribesByIdUser
}
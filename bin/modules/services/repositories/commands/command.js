'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');

const insertOneServices = async (document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('services');
    const result = await db.insertOne(document);
    return result;
}

const updateOneServices = async (param, document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('services');
    const result = await db.upsertOne(param ,document);
    return result;
}

const deleteOneServices = async (param) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('services');
    const result = await db.deleteOne(param);
    return result;
}

module.exports = {
    insertOneServices: insertOneServices,
    updateOneServices: updateOneServices,
    deleteOneServices: deleteOneServices
}
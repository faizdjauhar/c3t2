'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');

const findSubscribes = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('subscribes');
    const recordset = await db.findMany();
    return recordset;
}

const findSubscribesByIdUser = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('subscribes');
    const recordset = await db.findManyWithParam(parameter);
    return recordset;
}

const findOneSubscribes = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('subscribes');
    const recordset = await db.findOne(parameter);
    return recordset;
}

module.exports = {
    findSubscribes : findSubscribes,
    findSubscribesByIdUser: findSubscribesByIdUser,
    findOneSubscribes : findOneSubscribes
}
'use strict';

const Subscribes = require('./domain');

const postOneSubscribes = async (payload) => {
    const subscribes = new Subscribes();
    const postCommand = async (payload) => {
        return await subscribes.addNewSubscribes(payload);
    }
    return postCommand(payload);
}

const putOneSubscribes = async (id, payload) => {
    const subscribes = new Subscribes();
    const putCommand = async (id, payload) => {
        return await subscribes.updateDataSubscribes(id, payload);
    }
    return putCommand(id, payload);
}

const delOneSubscribes = async (id) => {
    const subscribes = new Subscribes();
    const putCommand = async (id) => {
        return await subscribes.deleteDataSubscribes(id);
    }
    return putCommand(id);
}

module.exports = {
    postOneSubscribes : postOneSubscribes,
    putOneSubscribes : putOneSubscribes,
    delOneSubscribes: delOneSubscribes
}
"use strict";

const generalServices = () => {
  const model = {
    service_id: ``,
    service_name: ``,
    service_price: ``,
    internet_speed: ``,
    total_channel: ``,
    call_min_available: ``,
    description: ``,
    service_status: ``,
    createdAt: ``,
    updatedAt: ``,
  };
  return model;
};

module.exports = {
  generalServices: generalServices,
};

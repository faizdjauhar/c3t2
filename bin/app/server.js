'use strict';

const restify = require('restify');
const project = require('../../package.json');
const basicAuth = require('../auth/basic_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
const corsMiddleware = require('restify-cors-middleware')
const usersHandler = require('../modules/users/handlers/api_handler');
const bantuanHandler = require('../modules/bantuan/handlers/api_handler');
const servicesHandler = require('../modules/services/handlers/api_handler');
const subscribesHandler = require('../modules/subscribes/handlers/api_handler');

let crossOrigin = (req,res,next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    return next();
}

const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: ['*'],
    allowHeaders: ['Origin, X-Requested-With, Content-Type, Accept, OPTIONS'],
    exposeHeaders: ['OPTIONS']
})

let AppServer = function(){
    const baseURL = '/api/v1/';

    this.server = restify.createServer({
        name: project.name + '-server',
        version: project.version
    });

    this.server.serverKey = '';
    this.server.pre(cors.preflight);
    this.server.use(cors.actual);
    this.server.use(restify.plugins.acceptParser(this.server.acceptable));
    this.server.use(restify.plugins.queryParser());
    this.server.use(restify.plugins.bodyParser());
    this.server.use(restify.plugins.authorizationParser());

    // required for basic auth
    this.server.use(basicAuth.init());
    this.server.use(crossOrigin);

    // anonymous can access the end point
    this.server.get('/', (req, res, next) => {
        wrapper.response(res, `success`, wrapper.data(`Index`), `This service is running properly`);
    });

    // authenticated client can access the end point
    // users
    this.server.get(baseURL + 'users', basicAuth.isAuthenticated, usersHandler.getUsers);
    this.server.get(baseURL + 'users/:idUser', basicAuth.isAuthenticated, usersHandler.getOneUsers);
    this.server.get(baseURL + 'users/:username/:password', basicAuth.isAuthenticated, usersHandler.login);
    this.server.post(baseURL + 'users', basicAuth.isAuthenticated, usersHandler.postOneUsers);
    this.server.put(baseURL + 'users/:idUser', basicAuth.isAuthenticated, usersHandler.putOneUsers);
    this.server.del(baseURL + 'users/:idUser', basicAuth.isAuthenticated, usersHandler.deleteOneUsers);

    // services
    this.server.get(baseURL + 'services', basicAuth.isAuthenticated, servicesHandler.getServices);
    this.server.get(baseURL + 'services/:serviceId', basicAuth.isAuthenticated, servicesHandler.getOneServices);
    this.server.post(baseURL + 'services', basicAuth.isAuthenticated, servicesHandler.postOneServices);
    this.server.put(baseURL + 'services/:serviceId', basicAuth.isAuthenticated, servicesHandler.putOneServices);
    this.server.del(baseURL + 'services/:serviceId', basicAuth.isAuthenticated, servicesHandler.delOneServices);

    // subscribes
    this.server.get(baseURL + 'subscribes', basicAuth.isAuthenticated, subscribesHandler.getSubscribes);
    this.server.get(baseURL + 'subscribes/:idSubscribes', basicAuth.isAuthenticated, subscribesHandler.getOneSubscribes);
    this.server.get(baseURL + 'subscribesby/:idUser', basicAuth.isAuthenticated, subscribesHandler.getSubscribesByIdUser);
    this.server.post(baseURL + 'subscribes', basicAuth.isAuthenticated, subscribesHandler.postOneSubscribes);
    this.server.put(baseURL + 'subscribes/:idSubscribes', basicAuth.isAuthenticated, subscribesHandler.putOneSubscribes);
    this.server.del(baseURL + 'subscribes/:idSubscribes', basicAuth.isAuthenticated, subscribesHandler.delOneSubscribes);

    // bantuan
    this.server.get(baseURL + 'bantuan', basicAuth.isAuthenticated, bantuanHandler.getBantuan);
    this.server.get(baseURL + 'bantuan/:idUser', basicAuth.isAuthenticated, bantuanHandler.getOneBantuan);
    this.server.post(baseURL + 'bantuan', basicAuth.isAuthenticated, bantuanHandler.postOneBantuan);
    this.server.put(baseURL + 'bantuan/:idUser', basicAuth.isAuthenticated, bantuanHandler.putOneBantuan);
    this.server.del(baseURL + 'bantuan/:idUser', basicAuth.isAuthenticated, bantuanHandler.deleteOneBantuan);
}
 
module.exports = AppServer;
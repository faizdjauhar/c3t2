'use strict';

const generalUsers = () => {
    const model = {
        idUser:``,
        email:``,
        username:``,
        password:``,
        name:``,
        phoneNumber:``,
        createdAt:``,
        updatedAt:``
    }
    return model;
}

module.exports = {
    generalUsers: generalUsers
}
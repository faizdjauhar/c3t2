'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');

const findServices = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('services');
    const recordset = await db.findMany();
    return recordset;
}

const findOneServices = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('services');
    const recordset = await db.findOne(parameter);
    return recordset;
}

module.exports = {
    findServices : findServices,
    findOneServices : findOneServices
}
"use strict";

const cluster = require("cluster");
const nconf = require("nconf");
const AppServer = require("./bin/app/server");
const configs = require("./bin/infra/configs/config");
const mongoConnectionPooling = require("./bin/helpers/databases/mongodb/connection");
const observer = require("./bin/modules/observer");
const { cpus } = require("os");
configs.initEnvironments(nconf);

const numCPUs = cpus().length;

if (cluster.isPrimary) {
  console.log(`Primary ${process.pid} is running`);

  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on("exit", (code, signal) => {
    if (signal) {
      console.log(`worker was killed by signal: ${signal}`);
    } else if (code !== 0) {
      console.log(`worker exited with error code: ${code}`);
    } else {
      console.log("worker success!");
    }
  });
} else {
  const appServer = new AppServer();
  const port = process.env.port || nconf.get("PORT") || 1337;
  appServer.server.listen(port, () => {
    mongoConnectionPooling.init();
    observer.init();
    console.log(
      "%s started, listening at %s",
      appServer.server.name,
      appServer.server.url
    );
  });

  console.log(`Worker ${process.pid} started`);
}

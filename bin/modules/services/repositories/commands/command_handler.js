"use strict";

const Services = require("./domain");

const postOneServices = async (payload) => {
  const services = new Services();
  const postCommand = async (payload) => {
    return await services.addNewServices(payload);
  };
  return postCommand(payload);
};

const putOneServices = async (id, payload) => {
  const services = new Services();
  const putCommand = async (id, payload) => {
    return await services.updateDataServices(id, payload);
  };
  return putCommand(id, payload);
};

const delOneServices = async (id) => {
  const services = new Services();
  const putCommand = async (id) => {
    return await services.deleteDataServices(id);
  };
  return putCommand(id);
};

module.exports = {
  postOneServices: postOneServices,
  putOneServices: putOneServices,
  delOneServices: delOneServices,
};

'use strict';

const generalSubscribes = () => {
    const model = {
        idSubscribes: ``,
        idUser:``,
        serviceId:``,
        renewTotal:``,
        createdAt:``,
        activeUntil:``,
        updatedAt:``
    }
    return model;
}

module.exports = {
    generalSubscribes: generalSubscribes
}
'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const validator = require('../utils/validator');
const queryHandler = require('../repositories/queries/query_handler');
const commandHandler = require('../repositories/commands/command_handler');

const getSubscribes = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParam(queryParam);

  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await queryHandler.getSubscribes(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}

const getOneSubscribes = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParam(queryParam);

  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await queryHandler.getOneSubscribes(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}

const getSubscribesByIdUser = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParam(queryParam);

  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await queryHandler.getSubscribesByIdUser(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}

const postOneSubscribes = async (req, res, next) => {
  const payload = req.body;
  const validateParam = await validator.isValidParam(payload);

  const postRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.postOneSubscribes(payload);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`, 200);
  }

  sendResponse(await postRequest(validateParam));
}

const putOneSubscribes = async (req, res, next) => {
  const id = req.params;
  const payload = req.body;
  const validateParam = await validator.isValidParam(payload);

  const putRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.putOneSubscribes(id, payload);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`, 200);
  }
  
  sendResponse(await putRequest(validateParam));
}

const delOneSubscribes = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParam(queryParam);

  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.delOneSubscribes(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}

module.exports = {
  getSubscribes: getSubscribes,
  getOneSubscribes: getOneSubscribes,
  getSubscribesByIdUser: getSubscribesByIdUser,
  postOneSubscribes: postOneSubscribes,
  putOneSubscribes: putOneSubscribes,
  delOneSubscribes: delOneSubscribes
}
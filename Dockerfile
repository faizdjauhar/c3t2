FROM node:16

# Create app directory
RUN mkdir -p /app
WORKDIR /app

# Install app dependencies
# COPY node_modules /app/

# Bundle app source
COPY . /app

EXPOSE 8080
CMD [ "npm", "start" ]
'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParam = async (payload) => {
    let constraints = {};
    let values = {};

    constraints[payload.idSubscribes] = {length: {minimum: 4}};
    constraints[payload.idUser] = {length: {minimum: 4}};
    constraints[payload.serviceId] = {length: {minimum: 4}};
    constraints[payload.activeUntil] = {length: {minimum: 4}};
    constraints[payload.createdAt] = {length: {minimum: 4}};

    values[payload.idSubscribes] = payload.idSubscribes;
    values[payload.idUser] = payload.idUser;
    values[payload.serviceId] = payload.serviceId;
    values[payload.renewTotal] = payload.renewTotal;
    values[payload.createdAt] = payload.createdAt;
    values[payload.activeUntil] = payload.activeUntil;
    values[payload.updatedAt] = payload.updatedAt;

    return await validateConstraints(values,constraints);
}

module.exports = {
    isValidParam: isValidParam
}
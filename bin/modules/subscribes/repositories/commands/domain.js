"use strict";

const model = require("./command_model");
const command = require("./command");
const validate = require("validate.js");

class Subscribes {
  async addNewSubscribes(payload) {
    const data = [payload];
    let view = model.generalSubscribes();
    view = data.reduce((accumulator, value) => {
      if (!validate.isEmpty(value.idSubscribes)) {
        accumulator.idSubscribes = value.idSubscribes;
      }
      if (!validate.isEmpty(value.idUser)) {
        accumulator.idUser = value.idUser;
      }
      if (!validate.isEmpty(value.serviceId)) {
        accumulator.serviceId = value.serviceId;
      }
      if (!validate.isEmpty(value.renewTotal)) {
        accumulator.renewTotal = value.renewTotal;
      }
      if (!validate.isEmpty(value.createdAt)) {
        accumulator.createdAt = value.createdAt;
      }
      if (!validate.isEmpty(value.activeUntil)) {
        accumulator.activeUntil = value.activeUntil;
      }
      if (!validate.isEmpty(value.updatedAt)) {
        accumulator.updatedAt = value.updatedAt;
      }
      return accumulator;
    }, view);
    const document = view;
    const result = await command.insertOneSubscribes(document);
    return result;
  }

  async updateDataSubscribes(param, payload) {
    const data = [payload];
    let view = model.generalSubscribes();
    view = data.reduce((accumulator, value) => {
      if (!validate.isEmpty(value.idSubscribes)) {
        accumulator.idSubscribes = value.idSubscribes;
      }
      if (!validate.isEmpty(value.idUser)) {
        accumulator.idUser = value.idUser;
      }
      if (!validate.isEmpty(value.serviceId)) {
        accumulator.serviceId = value.serviceId;
      }
      if (!validate.isEmpty(value.renewTotal)) {
        accumulator.renewTotal = value.renewTotal;
      }
      if (!validate.isEmpty(value.createdAt)) {
        accumulator.createdAt = value.createdAt;
      }
      if (!validate.isEmpty(value.activeUntil)) {
        accumulator.activeUntil = value.activeUntil;
      }
      if (!validate.isEmpty(value.updatedAt)) {
        accumulator.updatedAt = value.updatedAt;
      }
      return accumulator;
    }, view);
    const document = view;
    const result = await command.updateOneSubscribes(param, document);
    return result;
  }

  async deleteDataSubscribes(param) {
    const result = await command.deleteOneSubscribes(param);
    return result;
  }
}

module.exports = Subscribes;

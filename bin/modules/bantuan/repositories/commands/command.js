'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');

const insertOneBantuan = async (document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('bantuan');
    const result = await db.insertOne(document);
    return result;
}

const updateOneBantuan = async (param, document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('bantuan');
    const result = await db.upsertOne(param ,document);
    return result;
}

const deleteOneBantuan = async (param) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('bantuan');
    const result = await db.deleteOne(param);
    return result;
}

module.exports = {
    insertOneBantuan: insertOneBantuan,
    updateOneBantuan: updateOneBantuan,
    deleteOneBantuan: deleteOneBantuan
}
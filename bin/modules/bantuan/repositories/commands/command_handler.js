'use strict';

const Bantuan = require('./domain');

const postOneBantuan = async (payload) => {
    const bantuan = new Bantuan();
    const postCommand = async (payload) => {
        return await bantuan.addNewBantuan(payload);
    }
    return postCommand(payload);
}

const putOneBantuan = async (id, payload) => {
    const bantuan = new Bantuan();
    const putCommand = async (id, payload) => {
        return await bantuan.updateDataBantuan(id, payload);
    }
    return putCommand(id, payload);
}

const deleteOneBantuan = async (id) => {
    const bantuan = new Bantuan();
    const putCommand = async (id) => {
        return await bantuan.deleteDataBantuan(id);
    }
    return putCommand(id);
}

module.exports = {
    postOneBantuan : postOneBantuan,
    putOneBantuan : putOneBantuan,
    deleteOneBantuan: deleteOneBantuan
}
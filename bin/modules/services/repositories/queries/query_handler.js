'use strict';

const Services = require('./domain');

const getServices = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const services = new Services(queryParam);
        const result = await services.viewServices();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}

const getOneServices = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const services = new Services(queryParam);
        const result = await services.viewOneServices();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}

const getLogin = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const services = new Services(queryParam);
        const result = await services.login();
        return result;
    }
    
    const result = await getQuery(queryParam);
    return result;
}

module.exports = {
    getServices : getServices,
    getOneServices : getOneServices,
    getLogin: getLogin
}
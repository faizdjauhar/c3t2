'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');

const findBantuan = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('bantuan');
    const recordset = await db.findMany();
    return recordset;
}

const findOneBantuan = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('bantuan');
    const recordset = await db.findOne(parameter);
    return recordset;
}

module.exports = {
    findBantuan : findBantuan,
    findOneBantuan : findOneBantuan
}
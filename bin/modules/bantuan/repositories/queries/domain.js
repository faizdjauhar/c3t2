'use strict';

const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');

class Bantuan{
    constructor(param){
        this.idUser = param.idUser;
        this.username = param.username;
        this.content = param.content;
        this.createdAt = param.createdAt;
        this.updatedAt = param.updatedAt;
    }

    async viewBantuan(){
        const param = {};
        const result = await query.findBantuan(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }

    async viewOneBantuan(){
        const param = {"idUser":this.idUser};
        const result = await query.findOneBantuan(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }
}

module.exports = Bantuan;
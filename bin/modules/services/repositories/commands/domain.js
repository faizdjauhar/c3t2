"use strict";

const model = require("./command_model");
const command = require("./command");
const validate = require("validate.js");

class Services {
  async addNewServices(payload) {
    const data = [payload];
    let view = model.generalServices();
    view = data.reduce((accumulator, value) => {
      if (!validate.isEmpty(value.service_id)) {
        accumulator.service_id = value.service_id;
      }
      if (!validate.isEmpty(value.service_name)) {
        accumulator.service_name = value.service_name;
      }
      if (!validate.isEmpty(value.service_price)) {
        accumulator.service_price = value.service_price;
      }
      if (!validate.isEmpty(value.internet_speed)) {
        accumulator.internet_speed = value.internet_speed;
      }
      if (!validate.isEmpty(value.total_channel)) {
        accumulator.total_channel = value.total_channel;
      }
      if (!validate.isEmpty(value.call_min_available)) {
        accumulator.call_min_available = value.call_min_available;
      }
      if (!validate.isEmpty(value.description)) {
        accumulator.description = value.description;
      }
      if (!validate.isEmpty(value.service_status)) {
        accumulator.service_status = value.service_status;
      }
      if (!validate.isEmpty(value.createdAt)) {
        accumulator.createdAt = value.createdAt;
      }
      if (!validate.isEmpty(value.updatedAt)) {
        accumulator.updatedAt = value.updatedAt;
      }
      return accumulator;
    }, view);
    const document = view;
    const result = await command.insertOneServices(document);
    return result;
  }

  async updateDataServices(param, payload) {
    const data = [payload];
    let view = model.generalServices();
    view = data.reduce((accumulator, value) => {
      if (!validate.isEmpty(value.service_id)) {
        accumulator.service_id = value.service_id;
      }
      if (!validate.isEmpty(value.service_name)) {
        accumulator.service_name = value.service_name;
      }
      if (!validate.isEmpty(value.service_price)) {
        accumulator.service_price = value.service_price;
      }
      if (!validate.isEmpty(value.internet_speed)) {
        accumulator.internet_speed = value.internet_speed;
      }
      if (!validate.isEmpty(value.total_channel)) {
        accumulator.total_channel = value.total_channel;
      }
      if (!validate.isEmpty(value.call_min_available)) {
        accumulator.call_min_available = value.call_min_available;
      }
      if (!validate.isEmpty(value.description)) {
        accumulator.description = value.description;
      }
      if (!validate.isEmpty(value.service_status)) {
        accumulator.service_status = value.service_status;
      }
      if (!validate.isEmpty(value.createdAt)) {
        accumulator.createdAt = value.createdAt;
      }
      if (!validate.isEmpty(value.updatedAt)) {
        accumulator.updatedAt = value.updatedAt;
      }
      return accumulator;
    }, view);
    const document = view;
    const result = await command.updateOneServices(param, document);
    return result;
  }

  async deleteDataServices(param) {
    const result = await command.delOneServices(param);
    return result;
  }
}

module.exports = Services;

"use strict";

const query = require("./query");
const wrapper = require("../../../../helpers/utils/wrapper");

class Services {
  constructor(param) {
    this.service_id = param.service_id;
    this.service_name = param.service_name;
    this.service_price = param.service_price;
    this.internet_speed = param.internet_speed;
    this.total_channel = param.total_channel;
    this.call_min_available = param.call_min_available;
    this.description = param.description;
    this.service_status = param.service_status;
    this.createdAt = param.createdAt;
    this.updatedAt = param.updatedAt;
  }

  async viewServices() {
    const param = {};
    const result = await query.findServices(param);

    if (result.err) {
      return result;
    } else {
      return wrapper.data(result.data);
    }
  }

  async viewOneServices() {
    const param = { service_id: this.service_id };
    const result = await query.findOneServices(param);

    if (result.err) {
      return result;
    } else {
      return wrapper.data(result.data);
    }
  }
}

module.exports = Services;

"use strict";

const Mongo = require("../../../../helpers/databases/mongodb/db");
const config = require("../../../../infra/configs/global_config");

const insertOneSubscribes = async (document) => {
  const db = new Mongo(config.getDevelopmentDB());
  db.setCollection("subscribes");
  const result = await db.insertOne(document);
  return result;
};

const updateOneSubscribes = async (param, document) => {
  const db = new Mongo(config.getDevelopmentDB());
  db.setCollection("subscribes");
  const result = await db.upsertOne(param, document);
  return result;
};

const deleteOneSubscribes = async (param) => {
  const db = new Mongo(config.getDevelopmentDB());
  db.setCollection("subscribes");
  const result = await db.deleteOne(param);
  return result;
};

module.exports = {
  insertOneSubscribes: insertOneSubscribes,
  updateOneSubscribes: updateOneSubscribes,
  deleteOneSubscribes: deleteOneSubscribes,
};

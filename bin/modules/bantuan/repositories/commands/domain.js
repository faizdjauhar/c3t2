'use strict';

const model = require('./command_model');
const command = require('./command');
const validate = require('validate.js');

class Bantuan{

    async addNewBantuan(payload){
        const data = [payload];
        let view = model.generalBantuan();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.idUser)){accumulator.idUser = value.idUser;}
            if(!validate.isEmpty(value.username)){accumulator.username = value.username;} 
            if(!validate.isEmpty(value.content)){accumulator.name = value.name;}
            if(!validate.isEmpty(value.createdAt)){accumulator.createdAt = value.createdAt;}
            if(!validate.isEmpty(value.updatedAt)){accumulator.updatedAt = value.updatedAt;}            
            return accumulator;
        }, view);
        const document = view;
        const result = await command.insertOneBantuan(document);
        return result;
    }

    async updateDataBantuan(param, payload){
        const data = [payload];
        let view = model.generalBantuan();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.idUser)){accumulator.idUser = value.idUser;}
            if(!validate.isEmpty(value.username)){accumulator.username = value.username;} 
            if(!validate.isEmpty(value.content)){accumulator.name = value.name;}
            if(!validate.isEmpty(value.createdAt)){accumulator.createdAt = value.createdAt;}
            if(!validate.isEmpty(value.updatedAt)){accumulator.updatedAt = value.updatedAt;}            
            return accumulator;
        }, view);
        const document = view;
        const result = await command.updateOneBantuan(param, document);
        return result;
    }

    async deleteDataBantuan(param){
        const result = await command.deleteOneBantuan(param);
        return result;
    }
}

module.exports = Bantuan;
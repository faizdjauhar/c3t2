'use strict';

const Bantuan = require('./domain');

const getBantuan = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const bantuan = new Bantuan(queryParam);
        const result = await bantuan.viewBantuan();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}

const getOneBantuan = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const bantuan = new Bantuan(queryParam);
        const result = await bantuan.viewOneBantuan();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}

const getLogin = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const bantuan = new Bantuan(queryParam);
        const result = await bantuan.login();
        return result;
    }
    
    const result = await getQuery(queryParam);
    return result;
}

module.exports = {
    getBantuan : getBantuan,
    getOneBantuan : getOneBantuan,
    getLogin: getLogin
}
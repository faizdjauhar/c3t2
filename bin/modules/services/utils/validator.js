'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParam = async (payload) => {
    let constraints = {};
    let values = {};

    constraints[payload.serviceId] = {length: {minimum: 4}};
    constraints[payload.name] = {length: {minimum: 4}};
    constraints[payload.description] = {length: {minimum: 4}};
    constraints[payload.createdAt] = {length: {minimum: 4}};

    values[payload.serviceId] = payload.serviceId;
    values[payload.name] = payload.name;
    values[payload.price] = payload.price;
    values[payload.speed] = payload.speed;
    values[payload.totalChannel] = payload.totalChannel;
    values[payload.callTime] = payload.callTime;
    values[payload.description] = payload.description;
    values[payload.bonus] = payload.bonus;
    values[payload.createdAt] = payload.createdAt;
    values[payload.updatedAt] = payload.updatedAt;

    return await validateConstraints(values,constraints);
}

module.exports = {
    isValidParam: isValidParam
}
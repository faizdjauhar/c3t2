'use strict';

const model = require('./command_model');
const command = require('./command');
const validate = require('validate.js');

class Users{

    async addNewUsers(payload){
        const data = [payload];
        let view = model.generalUsers();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.idUser)){accumulator.idUser = value.idUser;}
            if(!validate.isEmpty(value.email)){accumulator.email = value.email;}
            if(!validate.isEmpty(value.username)){accumulator.username = value.username;}
            if(!validate.isEmpty(value.password)){accumulator.password = value.password;}  
            if(!validate.isEmpty(value.name)){accumulator.name = value.name;}
            if(!validate.isEmpty(value.phoneNumber)){accumulator.phoneNumber = value.phoneNumber;}
            if(!validate.isEmpty(value.createdAt)){accumulator.createdAt = value.createdAt;}
            if(!validate.isEmpty(value.updatedAt)){accumulator.updatedAt = value.updatedAt;}            
            return accumulator;
        }, view);
        const document = view;
        const result = await command.insertOneUsers(document);
        return result;
    }

    async updateDataUsers(param, payload){
        const data = [payload];
        let view = model.generalUsers();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.idUser)){accumulator.idUser = value.idUser;}
            if(!validate.isEmpty(value.email)){accumulator.email = value.email;}
            if(!validate.isEmpty(value.username)){accumulator.username = value.username;}
            if(!validate.isEmpty(value.password)){accumulator.password = value.password;}  
            if(!validate.isEmpty(value.name)){accumulator.name = value.name;}
            if(!validate.isEmpty(value.phoneNumber)){accumulator.phoneNumber = value.phoneNumber;}
            if(!validate.isEmpty(value.createdAt)){accumulator.createdAt = value.createdAt;}
            if(!validate.isEmpty(value.updatedAt)){accumulator.updatedAt = value.updatedAt;}            
            return accumulator;
        }, view);
        const document = view;
        const result = await command.updateOneUsers(param, document);
        return result;
    }

    async deleteDataUsers(param){
        const result = await command.deleteOneUsers(param);
        return result;
    }
}

module.exports = Users;